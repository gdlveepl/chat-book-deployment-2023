"use client";

import { collection, addDoc, getDoc, doc } from "firebase/firestore";
import { db, storage } from "@/firebase";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";

import { Button } from "@/components/ui/button";
import { Checkbox } from "@/components/ui/checkbox";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { toast } from "@/components/ui/use-toast";
import { useEffect, useState } from "react";
import Nav from "@/components/Nav";

const FormSchema = z.object({
  items: z.array(z.string()).refine((value) => value.some((item) => item), {
    message: "You have to select at least one item.",
  }),
});

export default function Home() {
  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
  });

  const [items, setItems] = useState<{ id: string; label: string }[]>([]);

  useEffect(() => {
    const fetchAmenitiesLabels = async () => {
      try {
        const amenitiesDoc = await getDoc(
          doc(db, "amenities", "amenitiesLabel")
        );
        const amenityLabels = amenitiesDoc.data();
        if (amenityLabels) {
          const amenityArray = Object.keys(amenityLabels).map((key) => ({
            id: key,
            label: amenityLabels[key],
          }));
          setItems(amenityArray);
        }
      } catch (error) {
        console.error("Error fetching amenity labels:", error);
      }
    };

    fetchAmenitiesLabels();
  }, []);

  function onSubmit(data: z.infer<typeof FormSchema>) {
    console.log("button submitted");
  }

  return (
    <main className="">
      <div className=" h-screen flex flex-row">
        <div className="px-5 w-full">
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
              <FormField
                control={form.control}
                name="items"
                render={() => (
                  <FormItem>
                    <div className="mb-4">
                      <FormLabel className="text-4xl">Add Amenities</FormLabel>
                      <FormDescription>
                        Select the items that will be included as room amenities
                      </FormDescription>
                    </div>
                    {items.map((item) => (
                      <FormItem key={item.id}>
                        <div className="flex flex-row items-start space-x-3 space-y-0">
                          <FormControl>
                            <Checkbox
                              checked={form
                                .getValues("items")
                                ?.includes(item.id)}
                              onCheckedChange={(checked) => {
                                const newItems = form.getValues("items")
                                  ? [...form.getValues("items")]
                                  : [];
                                if (checked) {
                                  newItems.push(item.id);
                                } else {
                                  const indexToRemove = newItems.indexOf(
                                    item.id
                                  );
                                  if (indexToRemove !== -1) {
                                    newItems.splice(indexToRemove, 1);
                                  }
                                }
                                form.setValue("items", newItems);
                              }}
                            />
                          </FormControl>
                          <FormLabel className="font-normal text-lg">
                            {item.label}
                          </FormLabel>
                        </div>
                      </FormItem>
                    ))}
                    <FormMessage />
                  </FormItem>
                )}
              />
              <Button className="text-xl">Submit</Button>
            </form>
          </Form>
        </div>
      </div>
    </main>
  );
}
