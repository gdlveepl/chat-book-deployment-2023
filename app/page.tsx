import Link from "next/link";

export default function Home() {
  return (
    <main className="">
      <div className="relative h-256 w-full overflow-hidden">
        <img
          src="home.jpg"
          alt="homepage"
          className="object-cover xl:h-[1500px] w-full"
        />
        <div className="top-[400px] left-1/2 flex flex-col items-center xl:absolute xl:top-[400px] xl:left-0 xl:right-0 xl:w-full p-10">
          <h1 className="text-center text-4xl xl:text-9xl font-bold text-primary xl:text-white shadow-xl">
            Looking for a Place to Stay?
          </h1>
          <Link
            href="/rooms"
            className=" text-primary xl:text-white text-2xl xl:text-6xl font-semibold bg-none py-2 px-8 border-4 xl:border-8  border-primary xl:border-black rounded-full transition duration-200 hover:bg-black hover:text-white mt-8"
          >
            Book Now!
          </Link>
        </div>
        <div>

        </div>
      </div>
    </main>
  );
}
