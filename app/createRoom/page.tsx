"use client";
import {
  getDownloadURL,
  getStorage,
  ref,
  uploadBytesResumable,
} from "firebase/storage";
import { useEffect, useState } from "react";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { db } from "@/firebase";
import { zodResolver } from "@hookform/resolvers/zod";
import { addDoc, collection } from "firebase/firestore";
import { useForm } from "react-hook-form";
import * as z from "zod";

import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { UserAuth } from "@/context/AuthContext";
import { useRouter } from "next/navigation";
import React from "react";
import { supabase } from "@/lib/supabase";
export default function Home() {
  const storage = getStorage();
  const storageRef = ref(storage);
  const [selectedImage, setSelectedImage] = useState<File | null>(null);
  const router = useRouter();
  const { user }: any = UserAuth();

  useEffect(() => {
    if (!user || user.supabase.userType === "guest") router.push("/");
  }, [user]);

  const formSchema = z.object({
    roomname: z.string().min(2).max(30),
    roomprice: z.preprocess(
      (a) => parseInt(z.string().parse(a)),
      z.number().positive().min(1)
    ),
    roomdetails: z.string(),
    roompax: z.string(),
    roomimage: z.any(),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      roomname: "",
      roomprice: 0,
      roomdetails: "",
      roompax: "",
      roomimage: null,
    },
  });

  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0] || null;
    setSelectedImage(file);
  };

  const onSubmit = async ({
    roomname,
    roomprice,
    roomdetails,
    roompax,
    roomimage,
  }: z.infer<typeof formSchema>) => {
    const { data, error } = await supabase
      .from("Room")
      .insert([
        {
          name: roomname,
          roomRate: roomprice,
          roomDetails: roomdetails,
          capacity: roompax,
          updatedAt: new Date(),
        },
      ])
      .select();

    console.log(data);

    // try {
    //   // Ask for confirmation before submitting
    //   const isConfirmed = window.confirm(
    //     "Are you sure you want to submit this room?"
    //   );

    //   if (!isConfirmed) {
    //     return; // Do nothing if the user cancels
    //   }
    //   // Upload image to Firebase Storage
    //   const storageRef = ref(storage, `roomImages/${roomname}-${Date.now()}`);
    //   const uploadTask = uploadBytesResumable(storageRef, roomimage);

    //   uploadTask.on(
    //     "state_changed",
    //     (snapshot) => {
    //       // You can handle the upload progress here if needed
    //       // const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    //       // console.log(`Upload is ${progress}% done`);
    //     },
    //     (error) => {
    //       console.error("Error uploading image:", error);
    //     },
    //     async () => {
    //       // Image uploaded successfully, get download URL
    //       const downloadURL = await getDownloadURL(uploadTask.snapshot.ref);
    //       console.log("Download URL:", downloadURL);

    //       // Add the form data to the Firestore collection
    //       const docRef = await addDoc(collection(db, "rooms"), {
    //         roomname,
    //         roomprice,
    //         roomdetails,
    //         roompax,
    //         roomimage: downloadURL, // Store the download URL in Firestore
    //         // ... (add other fields as needed)
    //       });

    //       console.log("Document written with ID: ", docRef.id);
    //       form.reset({
    //         roomname: "",
    //         roomprice: 0,
    //         roomdetails: "",
    //         roompax: "",
    //         roomimage: null,
    //       });
    //       setSelectedImage(null); // Optionally, reset the image preview state
    //     }
    //   );
    // } catch (error) {
    //   console.error("Error adding document: ", error);
    // }
  };

  return (
    <main className="">
      <div className=" h-screen flex flex-row">
        <div className="px-5 w-full">
          <h1 className="text-5xl text-black font-semibold">Create Rooms</h1>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
              <FormField
                control={form.control}
                name="roomname"
                render={({ field }) => (
                  <FormItem className="w-1/3">
                    <FormLabel className="text-lg">Room Name</FormLabel>
                    <FormControl>
                      <Input placeholder="Room 101" {...field} />
                    </FormControl>
                    <FormDescription>
                      This will be the display name of the room.
                    </FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="roomprice"
                render={({ field }) => (
                  <FormItem className="w-1/3">
                    <FormLabel className="text-lg">Room Price</FormLabel>
                    <FormControl>
                      <Input placeholder="1000" {...field} />
                    </FormControl>
                    <FormDescription>
                      This will be the rate of the room per night
                    </FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="roomdetails"
                render={({ field }) => (
                  <FormItem className="w-1/3">
                    <FormLabel className="text-lg">Room Details</FormLabel>
                    <FormControl>
                      <Textarea
                        className="whitespace-normal"
                        placeholder="Indulge in the lap of luxury with our exclusive Luxury Suite. This opulent space is meticulously designed for the discerning traveler who seeks nothing but the best. From exquisite furnishings to state-of-the-art amenities, every detail is crafted to ensure an unforgettable experience."
                        {...field}
                      />
                    </FormControl>
                    <FormDescription>
                      This will be the details of the room
                    </FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="roomimage"
                render={({ field }) => (
                  <FormItem className="w-1/3">
                    <FormLabel className="text-lg">Room Images</FormLabel>
                    <FormControl>
                      <Input
                        id="picture"
                        type="file"
                        onChange={(e) => {
                          field.onChange(e); // Trigger the field's onChange
                          handleImageChange(e); // Handle image change separately
                        }}
                      />
                    </FormControl>
                    {selectedImage && (
                      <div>
                        <p>Preview:</p>
                        <img
                          src={URL.createObjectURL(selectedImage)}
                          alt="Preview"
                          style={{ maxWidth: "100%", maxHeight: "200px" }}
                        />
                      </div>
                    )}
                    <FormDescription>
                      This will be the images displayed for the room
                    </FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="roompax"
                render={({ field }) => (
                  <FormItem className="w-1/3">
                    <FormLabel className="text-lg">
                      Maximum Number of Guests
                    </FormLabel>
                    <FormControl>
                      <Input placeholder="5" {...field} />
                    </FormControl>
                    <FormDescription>
                      This will be the maximum allowed guests in the room
                    </FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <Button type="submit">Next</Button>
            </form>
          </Form>
        </div>
      </div>
    </main>
  );
}
