"use client";
import { Button } from "@/components/ui/button"
import { useState } from "react";
import { useRouter } from 'next/navigation';
import { useSearchParams } from "next/navigation";
import { toast } from "@/components/ui/use-toast";
import { UserAuth } from "@/context/AuthContext";

import * as React from 'react';
import {
    Card,
    CardContent,
    CardDescription,
    CardHeader,
    CardTitle,
} from "@/components/ui/card"
export default function Home() {
    const router = useRouter();
    const searchParams = useSearchParams();
    const [bookingDetails] = useState({
        rate: searchParams.get("roomRate"),
        id: searchParams.get("roomId"),
        name: searchParams.get("roomName"),
        roomDetails: searchParams.get("roomDetails"),
    });
    const { user }: any = UserAuth();
    return (
        <div className='p-10 mx-96'>
            <div className="flex justify-between items-center">
                <div>
                    <h1 className='text-3xl font-bold'>
                        {bookingDetails.name}
                    </h1>
                    <p>
                        {"4.32 > # of reviews"}
                    </p>
                </div>
                <div className="flex justify-between items-center">
                    <div className="px-4">
                        <h1 className=' text-xl font-bold'>
                            ₱{bookingDetails.rate}
                        </h1>
                        <h1 className=' text-lg'>
                            per night
                        </h1>
                    </div>
                    <Button className="text-2xl font-bold p-2" onClick={() => {
                        if (user) {
                            router.push(
                                `/booking?roomRate=${bookingDetails.rate}&roomId=${bookingDetails.id}&roomName=${bookingDetails.name}`
                            );
                        } else {
                            toast({
                                title: "Please sign in to continue",
                                variant: "destructive",
                            });
                            router.push(
                                `/rooms/roomdetails?roomRate=${bookingDetails.rate}&roomId=${bookingDetails.id}&roomName=${bookingDetails.name}&roomDetails=${bookingDetails.roomDetails}`
                            )
                        }
                    }}>Book Now</Button>
                </div>
            </div>
            <div className=" py-5 h-[1000px] w-full overflow-clip">
                <img src='/rooms.jpg' alt='homepage' className=' object-cover h-[1000px] w-full'></img>
            </div>
            <div className="py-5">
                <div className="w-2/3">
                    <h1 className="text-lg font-semibold pb-2"> About This Space</h1>
                    <p className="pb-5 text-justify text-lg border-b border-primary"> {bookingDetails.roomDetails} </p>
                    <h1 className="text-lg font-semibold pb-2">What this room offer</h1>
                    <ul className="pb-5 border-b border-primary">
                        <li className="px-1">
                            Amenities
                        </li>
                        <li className="px-1">
                            Amenities
                        </li>
                        <li className="px-1">
                            Amenities
                        </li>
                        <li className="px-1">
                            Amenities
                        </li>

                    </ul>
                    <h1 className="text-lg font-semibold pb-2"> # of Reviews</h1>

                    <div className="flex">
                        <div className="w-1/2 px-2">
                            <Card>
                                <CardHeader>
                                    <CardTitle>Guest Name</CardTitle>
                                    <CardDescription>Manila, Philippines</CardDescription>
                                    <h1>4/5 Date of Stay</h1>
                                </CardHeader>
                                <CardContent>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu sollicitudin velit. Aenean vel sem scelerisque, scelerisque risus vitae, faucibus lacus.</p>
                                </CardContent>
                            </Card>
                        </div>
                        <div className="w-1/2 px-2">
                            <Card>
                                <CardHeader>
                                    <CardTitle>Guest Name</CardTitle>
                                    <CardDescription>Manila, Philippines</CardDescription>
                                    <h1>5/5 Date of Stay</h1>
                                </CardHeader>
                                <CardContent>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu sollicitudin velit. Aenean vel sem scelerisque, scelerisque risus vitae, faucibus lacus.</p>
                                </CardContent>
                            </Card>
                        </div>
                    </div>
                </div>
                <div>

                </div>
            </div>
        </div>
    )
}
