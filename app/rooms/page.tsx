"use client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import { createClient } from "@supabase/supabase-js";
import { supabase } from "@/lib/supabase";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { useRouter } from "next/navigation";

interface Room {
  id: number;
  name: string;
  roomDetails: string;
  roomRate: string;
}

const RoomCard: React.FC<{ room: Room }> = ({ room }) => {
  const router = useRouter();
  return (
    <Card className=" w-11/12">
      <CardHeader>
        <CardTitle>{room.name}</CardTitle>
      </CardHeader>
      <CardContent>
        <img
          src="rooms.jpg"
          alt="Room Img"
          className="object-cover h-96 w-full"
        />
        <p>{room.roomDetails}</p>
      </CardContent>
      <CardFooter className=" justify-between">
        <p className=" font-semibold text-xl">{room.roomRate}</p>
        <Button
          onClick={() => {
            router.push(
              `/rooms/roomdetails?roomRate=${room.roomRate}&roomId=${room.id}&roomName=${room.name}&roomDetails=${room.roomDetails}`
            );
          }}
        >
          View Room
        </Button>
      </CardFooter>
    </Card>
  );
};

export default function Home() {
  const [rooms, setRooms] = useState<any[] | null>(null);
  useEffect(() => {
    const getRooms = async () => {
      let { data: Room, error } = await supabase
        .from("Room")
        .select("*")
        .range(0, 100);
      return Room;
    };
    getRooms().then((rooms) => {
      setRooms(rooms);
    });
  }, []);

  return (
    <div>
      <div className="h-96 w-full overflow-clip shadow-2xl">
        <img
          src="rooms.jpg"
          alt="homepage"
          className=" object-cover h-96 w-full"
        ></img>
      </div>
      <div className="p-10 2xl:mx-80">
        <h1 className="text-6xl font-bold text-center">HAPPY HOMES ROOMS</h1>
        <div className="grid grid-cols-1 xl:grid-cols-3 gap-8 p-10">
          {rooms?.map((room) => (
            <RoomCard key={room.id} room={room} />
          ))}
        </div>
      </div>
    </div>
  );
}
