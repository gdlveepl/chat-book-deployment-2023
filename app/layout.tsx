import Foot from "@/components/Foot";
import Nav from "@/components/Nav";
import { ThemeProvider } from "@/components/theme-provider";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { AuthContextProvider } from "@/context/AuthContext";
import { Toaster } from "@/components/ui/toaster";
const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Chat Book",
  description: "A Condo-Tel Booking Site",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <ThemeProvider attribute="class" defaultTheme="system">
          <AuthContextProvider>
            <Nav />
            <div>{children}</div>
            <Toaster />
            <Foot />
          </AuthContextProvider>
        </ThemeProvider>
      </body>
    </html>
  );
}
