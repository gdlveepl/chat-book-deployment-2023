"use client";
import React, { useEffect } from "react";
import { useState } from "react";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";

import { Checkbox } from "@/components/ui/checkbox";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { toast } from "@/components/ui/use-toast";
import { cn } from "@/lib/utils";
import { format } from "date-fns";
import { CalendarIcon } from "lucide-react";
import { Calendar } from "@/components/ui/calendar";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import moment from "moment";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { Input } from "@/components/ui/input";
import { useRouter } from "next/navigation";
import { UserAuth } from "@/context/AuthContext";
import { useSearchParams } from "next/navigation";
import { DatePickerWithRange } from "@/components/ui/date-range";
import { DateRange } from "react-day-picker";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Label } from "@/components/ui/label";
import { supabase } from "@/lib/supabase";

const formSchema = z.object({
  number: z.string().min(2, {
    message: "Number must be at least 11 characters.",
  }),
  date: z.any(),
});

export default function Home() {
  const [date, setNewDate] = useState<DateRange | undefined>({
    from: new Date(),
    to: new Date(),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      number: "",
    },
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    const createBooking = async () => {
      const { data, error } = await supabase
        .from("Booking")
        .insert([
          {
            dateTimeStart: moment(date?.from).toISOString(),
            dateTimeEnd: moment(date?.to).toISOString(),
            totalDays,
            total:
              parseFloat(bookingDetails.rate ? bookingDetails.rate : "0") *
              totalDays,
            roomId: bookingDetails.id,
            userId: user.supabase.id,
          },
        ])
        .select();
    };

    createBooking();
  }
  const router = useRouter();
  const searchParams = useSearchParams();
  const { user }: any = UserAuth();

  const [bookingDetails] = useState({
    rate: searchParams.get("roomRate"),
    id: searchParams.get("roomId"),
    name: searchParams.get("roomName"),
  });

  const [totalDays, setTotalDays] = useState(0);

  useEffect(() => {
    if (date) setTotalDays(moment(date.to).diff(moment(date.from), "days"));
  }, [date]);

  return (
    <div className="mx-[500px] p-12 h-[1150px]">
      <div className="flex flex-row">
        <div className="w-full">
          <Card>
            <CardHeader>
              <CardTitle>Room "{bookingDetails.name}" Booking</CardTitle>
            </CardHeader>
            <CardContent className="py-5">
              <RadioGroup className="py-2" defaultValue="option-one">
                <div className="flex items-center space-x-2">
                  <RadioGroupItem value="option-one" id="option-one" />
                  <Label htmlFor="option-one">Credit Card</Label>
                </div>
                <div className="flex items-center space-x-2">
                  <RadioGroupItem value="option-two" id="option-two" />
                  <Label htmlFor="option-two">Paypal</Label>
                </div>
                <div className="flex items-center space-x-2">
                  <RadioGroupItem value="option-three" id="option-three" />
                  <Label htmlFor="option-two">GCash/Paymaya</Label>
                </div>
              </RadioGroup>
              <Form {...form}>
                <form
                  onSubmit={form.handleSubmit(onSubmit)}
                  className="space-y-8 w-1/2"
                >
                  <FormField
                    control={form.control}
                    name="number"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Phone Number</FormLabel>
                        <FormControl>
                          <Input placeholder="09xxxxxxxxx" {...field} />
                        </FormControl>
                        <FormDescription>
                          Please input phone number
                        </FormDescription>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="date"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Start and End</FormLabel>
                        <FormControl>
                          <DatePickerWithRange {...{ setNewDate }} />
                        </FormControl>
                        <FormDescription>Please Select Dates</FormDescription>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <Button type="submit" className="text-2xl">
                    Confirm & Book
                  </Button>
                </form>
              </Form>
            </CardContent>
            <CardFooter>
              <div className="py-2">
                <CardHeader>
                  <CardTitle>Price</CardTitle>
                </CardHeader>
                <CardContent className="flex justify-between">
                  <p className="text-xl font-semibold">Accomodation Charges:</p>
                  <p className="text-xl font-bold">
                    {bookingDetails.rate} * {totalDays} =
                    {parseFloat(
                      bookingDetails.rate ? bookingDetails.rate : "0"
                    ) * totalDays}{" "}
                  </p>
                </CardContent>
                <CardFooter>
                  <p>Price include all local taxes</p>
                </CardFooter>
              </div>
            </CardFooter>
          </Card>
        </div>
      </div>
    </div>
  );
}
