"use client";
import {
  GoogleAuthProvider,
  onAuthStateChanged,
  signInWithRedirect,
  signOut,
} from "firebase/auth";
import { createContext, useContext, useEffect, useState } from "react";

import { useToast } from "@/components/ui/use-toast";
import { auth } from "@/firebase";
import { supabase } from "@/lib/supabase";

const AuthContext: any = createContext({
  user: {},
  googleSignIn: () => {},
  logOut: () => {},
});

export const AuthContextProvider = ({ children }: any) => {
  const { toast } = useToast();
  const [user, setUser] = useState<any>(null);

  const googleSignIn = async () => {
    const provider = new GoogleAuthProvider();
    await signInWithRedirect(auth, provider);
  };

  const logOut = async () => {
    await signOut(auth)
      .then(() => {
        setUser(null);
        toast({
          description: "Signed Out Successfully",
        });
      })
      .catch((error) =>
        toast({
          title: "Something Went Wrong",
          description: error.toString(),
          variant: "destructive",
        })
      );
  };

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
      if (currentUser && !user) {
        const checkExisting = async (uid: string) => {
          const user = await supabase.from("User").select("*").eq("id", uid);
          if (!user?.data?.length) {
            const fullName: string = currentUser?.displayName
              ? currentUser?.displayName
              : "";
            const name =
              fullName.indexOf(" ") > -1 ? fullName.split(" ") : [fullName, ""];

            const { data, error } = await supabase
              .from("User")
              .insert([
                {
                  id: uid,
                  firstName: name[0],
                  lastName: name[1],
                  email: currentUser?.email,
                  password: currentUser?.uid,
                  updatedAt: new Date(),
                },
              ])
              .select();
          }
          setUser({
            ...currentUser,
            supabase: user.data ? user.data[0] : null,
          });
        };
        checkExisting(currentUser ? currentUser.uid.toString() : "");
      }
    });

    return () => unsubscribe();
  }, [user]);

  return (
    <AuthContext.Provider value={{ user, googleSignIn, logOut }}>
      {children}
    </AuthContext.Provider>
  );
};

export const UserAuth = () => {
  return useContext(AuthContext);
};
