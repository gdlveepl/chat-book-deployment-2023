// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDrGUOLDeniPLcEEVC0nfe5kzN5YC0ERGQ",
  authDomain: "chat-book-2023-686bf.firebaseapp.com",
  projectId: "chat-book-2023-686bf",
  storageBucket: "chat-book-2023-686bf.appspot.com",
  messagingSenderId: "790313280363",
  appId: "1:790313280363:web:df879c1653adecd1f0622e",
  measurementId: "G-QWBVNNCHP1",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// const analytics = getAnalytics(app);

export const storage = getStorage(app);

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app);

// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
