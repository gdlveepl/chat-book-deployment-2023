"use client";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { auth, db } from "@/firebase";
import { zodResolver } from "@hookform/resolvers/zod";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { useToast } from "./ui/use-toast";
import { addDoc, collection } from "firebase/firestore";

const SignUp = ({ onSuccess }: any) => {
  const { toast } = useToast();

  const formSchema = z
    .object({
      email: z.string().email(),
      password: z
        .string()
        .min(10, "Password must be at least 10")
        .max(20, "Maximum of 20 characters only"),
      confirmPassword: z.string().min(5).max(20),
      firstName: z.string().min(2).max(17),
      lastName: z.string().min(2).max(17),
      contactNumber: z.string().min(6).max(11),
    })
    .refine((data) => data.password === data.confirmPassword, {
      message: "Passwords do not match",
      path: ["confirmPassword"],
    });

  const onSubmit = async ({
    email,
    password,
    confirmPassword,
    firstName,
    lastName,
    contactNumber,
  }: z.infer<typeof formSchema>) => {
    await createUserWithEmailAndPassword(auth, email, password)
      .then(async (userCredential) => {
        try {
          // Add the form data to the Firestore collection
          const docRef = await addDoc(collection(db, "users"), {
            uid: userCredential.user.uid,
            firstName,
            lastName,
            contactNumber,
          });
          toast({
            description: "Registered Successfully",
          });
          onSuccess();
        } catch (error: any) {
          toast({
            title: "Something Went Wrong",
            description: error.toString(),
            variant: "destructive",
          });
        }
      })
      .catch((error) => {
        toast({
          title: "Something Went Wrong",
          description: error.toString(),
          variant: "destructive",
        });
      });
  };

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      password: "",
      firstName: "",
      lastName: "",
      contactNumber: "",
      confirmPassword: "",
    },
  });
  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-2">
        <FormField
          control={form.control}
          name="email"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Email</FormLabel>
              <FormControl>
                <Input placeholder="email" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Password</FormLabel>
              <FormControl>
                <Input type="password" placeholder="Password" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="confirmPassword"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Confirm Password</FormLabel>
              <FormControl>
                <Input
                  type="password"
                  placeholder="Confirm Password"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="firstName"
          render={({ field }) => (
            <FormItem>
              <FormLabel>First Name</FormLabel>
              <FormControl>
                <Input placeholder="First Name" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="lastName"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Last Name</FormLabel>
              <FormControl>
                <Input placeholder="Last Name" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="contactNumber"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Contact Number</FormLabel>
              <FormControl>
                <Input placeholder="Contact Number" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button type="submit" className="w-full text-xl">
          Submit
        </Button>
      </form>
    </Form>
  );
};

export default SignUp;
