"use client";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { UserAuth } from "@/context/AuthContext";
import { auth } from "@/firebase";
import { zodResolver } from "@hookform/resolvers/zod";
import { signInWithEmailAndPassword } from "firebase/auth";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { useToast } from "./ui/use-toast";
const Login = ({ onSuccess }: any) => {
  const formSchema = z.object({
    email: z.string().email(),
    password: z
      .string()
      .min(10, "Password must be at least 10")
      .max(20, "Maximum of 20 characters only"),
  });

  const { toast } = useToast();

  const onSubmit = async ({ email, password }: z.infer<typeof formSchema>) => {
    await signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        toast({
          description: "Signed In Successfully",
        });
        onSuccess();
      })
      .catch((error) => {
        toast({
          title: "Something Went Wrong",
          description: error.toString(),
          variant: "destructive",
        });
      });
  };

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  });

  const { googleSignIn }: any = UserAuth();

  const handleSignIn = async () => {
    try {
      await googleSignIn();
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-2">
        <FormField
          control={form.control}
          name="email"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Email</FormLabel>
              <FormControl>
                <Input placeholder="Email" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Password</FormLabel>
              <FormControl>
                <Input type="password" placeholder="password" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <div className="flex justify-between p-2">
          <Button type="submit" className="text-xl">
            Login
          </Button>
          <Button type="button" className="text-xl" onClick={handleSignIn}>
            Sign In With Google
          </Button>
        </div>
      </form>
    </Form>
  );
};

export default Login;
