"use client";

import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { UserAuth } from "@/context/AuthContext";
import { LogOut } from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import Login from "./Login";
import SignUp from "./SignUp";
import { ModeToggle } from "./ui/toggle-mode";
import { useRouter } from 'next/navigation';

const Nav = () => {
  const [modal, setModal] = useState(false);
  const onSuccess = () => setModal(false);
  const { user, logOut }: any = UserAuth();
  const router = useRouter();

  return (
    <nav className="w-full flex sticky top-0 z-50 items-center justify-between bg-amber-200 dark:bg-amber-700 p-10 shadow-md h-20">
      <ul className="flex justify-between items-center">
        <li>
          <Link href="/">
            <Image
              src="/chatbook-logo-black.png"
              alt="Logo"
              className="dark:invert"
              width={80}
              height={80}
              priority
            />
          </Link>
        </li>
        <li className=" dark:invert list-none px-5">
          <Link
            href="/"
            className="text-black text-xl font-semibold transition duration-200 hover:text-white"
          >
            Home
          </Link>
        </li>
        <li className="dark:invert list-none px-5">
          <Link
            href="/rooms"
            className="text-black text-xl font-semibold transition duration-200 hover:text-white"
          >
            Rooms & Rates
          </Link>
        </li>
        <li className="dark:invert list-none px-5">
          <Link
            href="/about"
            className="text-black text-xl font-semibold transition duration-200 hover:text-white"
          >
            About us
          </Link>
        </li>
        <li className="dark:invert list-none px-5">
          <Link
            href="/contact"
            className="text-black text-xl font-semibold transition duration-200 hover:text-white"
          >
            Contact Us
          </Link>
        </li>
        {user && user.supabase?.userType !== "guest" ? (
          <li className="dark:invert list-none px-5">
            <Link
              href="/createRoom"
              className="text-black text-xl font-semibold transition duration-200 hover:text-white"
            >
              Create Room
            </Link>
          </li>
        ) : (
          <></>
        )}
      </ul>
      <ul className="flex justify-between items-center gap-3">
        {user ? (
          <li className="flex">
            <DropdownMenu>
              <DropdownMenuTrigger>
                <Avatar>
                  <AvatarImage src="https://github.com/shadcn.png" />
                  <AvatarFallback>CN</AvatarFallback>
                </Avatar>
              </DropdownMenuTrigger>
              <DropdownMenuContent>
                <DropdownMenuLabel>{user.email}</DropdownMenuLabel>
                <DropdownMenuSeparator />
                <DropdownMenuItem onClick={() => {
                  router.push(
                    `/user`
                  );
                  }}>Profile
                </DropdownMenuItem>
                <DropdownMenuItem onClick={() => logOut()}>
                  <LogOut className="mr-2 h-4 w-4" />
                  <span>Log out</span>
                </DropdownMenuItem>
              </DropdownMenuContent>
            </DropdownMenu>
          </li>
        ) : (
          <li>
            <Dialog open={modal}>
              <DialogTrigger asChild>
                <Button onClick={() => setModal(true)} variant="outline">
                  Login/Signup
                </Button>
              </DialogTrigger>
              <DialogContent>
                <DialogHeader>
                  <DialogDescription>
                    <Tabs defaultValue="logIn" className="w-full">
                      <TabsList>
                        <TabsTrigger value="signUp">Sign Up</TabsTrigger>
                        <TabsTrigger value="logIn">Login</TabsTrigger>
                      </TabsList>
                      <TabsContent value="signUp">
                        <SignUp {...{ onSuccess }} />
                      </TabsContent>
                      <TabsContent value="logIn">
                        <Login {...{ onSuccess }} />
                      </TabsContent>
                    </Tabs>
                  </DialogDescription>
                </DialogHeader>
              </DialogContent>
            </Dialog>
          </li>
        )}
        <li>
          <ModeToggle />
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
